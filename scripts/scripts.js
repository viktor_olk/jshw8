// * Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

const input_1 = document.getElementById("input_1"),
  input_2 = document.getElementById("input_2"),
  button_1 = document.getElementById("input_button");

button_1.onclick = function () {
  let val1 = input_1.value,
    val2 = input_2.value;
  input_1.value = val2;
  input_2.value = val1;
};

// * Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.
const block = document.getElementById("block");
const [...divs] = block.getElementsByTagName("div");
divs.forEach((item) => {
  item.style.backgroundColor = "red";
});

// * Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна
// згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після
// переміщення інформації
const textarea = document.getElementById("textarea"),
  button_2 = document.getElementById("button_2");
button_2.onclick = function () {
  let val = textarea.value;
  let newDiv = document.createElement("div");
  newDiv.classList.add("divEx_2");
  newDiv.append(val);
  button_2.after(newDiv);
  textarea.value = "";
};

// * Створіть картинку та кнопку з назвою "Змінити картинку"
// зробіть так щоб при завантаженні сторінки була картинка
// https://itproger.com/img/courses/1476977240.jpg
// При натисканні на кнопку вперше картинка замінилася на
// https://itproger.com/img/courses/1476977488.jpg
// при другому натисканні щоб картинка замінилася на
// https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png

const img = document.getElementById("img");
const button_3 = document.getElementById("button_3");
let count = 1;
button_3.onclick = function () {
  if (count === 1) {
    img.setAttribute("src", "https://itproger.com/img/courses/1476977488.jpg");
    img.setAttribute("alt", "Картинка по першому клику");
    count += 1;
  } else if (count === 2) {
    img.setAttribute(
      "src",
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png"
    );
    img.setAttribute("alt", "Картинка по другому клику");
    count += 1;
  } else {
    img.setAttribute("src", "https://itproger.com/img/courses/1476977240.jpg");
    img.setAttribute("alt", "Картинка за замовчуванням");
    count = 1;
  }
};

// * Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав

const container = document.getElementById("container"),
  [...paragraphs] = container.getElementsByTagName("p");
paragraphs.forEach((item) => {
  item.onclick = () => {
    item.remove();
  };
});

// ***Додаткове ДЗ необов'язково***

// Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
// При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript
// При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При натисканні на конкретне коло - це коло повинен зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.

const button_4 = document.getElementById("button_4"),
  inputDiam = document.createElement("input"),
  button_5 = document.createElement("button");
inputDiam.setAttribute("placeholder", "Введи діаметр від 10 до 50)");

button_4.onclick = () => {
  button_4.after(inputDiam);
  inputDiam.after(button_5);
  button_5.append("Намалювати");
  button_4.remove();
};

button_5.onclick = () => {
  if (inputDiam.value >= 10 && inputDiam.value <= 50) {
    let count = 0;
    while (count < 100) {
      let circle = document.createElement("div");
      circle.classList.add("circle");
      circle.style.width = `${inputDiam.value}px`;
      circle.style.height = `${inputDiam.value}px`;
      circle.style.backgroundColor = `hsl(${Math.floor(
        Math.random() * 360
      )}, 100%, 50%)`;
      circle.onclick = () => {
        circle.remove();
      };
      button_5.after(circle);
      count++;
    }

    inputDiam.remove();
    button_5.remove();
  }
};
